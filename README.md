# DishDash App - README

Welcome to the DishDash App! This Android application allows users to browse and discover various recipes, view recipe details, and interact with the recipes by adding reviews. This document provides instructions on building, testing, analyzing, running, and using the mobile app.

## Table of Contents

- [Requirements](#requirements)
- [Getting Started](#getting-started)
  - [Building the App](#building-the-app)
  - [Testing the App](#testing-the-app)
  - [Analyzing the App](#analyzing-the-app)
- [Running the App](#running-the-app)
- [Using the App](#using-the-app)

## Requirements

- Android Studio (pre-installed in the Virtual Development environment)
- Android Emulator (pre-installed in the Virtual Development environment)
- Or Android Device minimum API 28: Android 9(Pie)

## Getting Started

To get started with the DishDash App, follow the steps below.

### Building the App

1. Clone this repository or download project to your local machine.
2. Open Android Studio on the Virtual Development environment.
3. Click on "Open an existing Android Studio project."
4. Navigate to the location where you cloned the repository and select the project folder.
5. Android Studio will import the project, and you should see it in the IDE.

### Testing the App

Instrumented tests (Espresso) are used to test the app's behavior on an Android device or emulator. To run instrumented tests:

1. Open Android Studio.
2. Navigate to the `ExampleInstrumentedTest` class located in the `androidTest` directory of the app module.
3. Right-click on the class name and select "Run ExampleInstrumentedTest" from the context menu.

### Analyzing the App

Android Studio provides tools to analyze the app's performance and find potential issues. To analyze the app, follow these steps:

1. In Android Studio, open the project if not already opened.
2. Click on "Build" in the top menu and select "Analyze APK..."
3. Choose the APK to analyze (usually located in the "build/outputs/apk/" directory).
4. Android Studio will display a report with information about the APK, including performance and security.

## Running the App

To run the DishDash App on the Android Emulator, follow these steps:

1. In Android Studio, open the project if not already opened.
2. Click on "Run" in the top menu and select "Run 'app'."
3. Choose the desired emulator from the list of available devices.
4. Android Studio will build the app and run it on the selected emulator.

## Using the App

The DishDash app is a recipe-sharing platform. Upon launching the app, you will be presented with a login screen. If you don't have an account, you can click on the "Sign up" option to create a new account. Otherwise, you can log in with your existing credentials.

Once logged in, you can browse and search for recipes shared by other users. You can view recipe details, including ingredients and preparation steps. If you are the owner of a recipe, you can edit or delete it using the appropriate options.

To share a new recipe, you can click on the "Add Recipe" button and provide all the necessary details, such as the recipe name, category, ingredients, and steps.

Enjoy using the DishDash app to explore, share, and discover delicious recipes from various cuisines!

---
