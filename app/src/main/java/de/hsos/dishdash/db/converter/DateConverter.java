package de.hsos.dishdash.db.converter;

import androidx.room.TypeConverter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateConverter {

    @TypeConverter
    public static Date fromTimestamp(String value) {
        if (value == null) {
            return null;
        } else {
            try {
                return new SimpleDateFormat("MM dd, yyyy", Locale.getDefault()).parse(value);
            } catch (ParseException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    /*
    public static Date fromTimestamp(String value) {
        return value != null ? new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(value) : null;
    }
    */

    @TypeConverter
    public static String dateToTimestamp(Date date) {
        return date != null ? new SimpleDateFormat("MM dd, yyyy", Locale.getDefault()).format(date) : null;
    }

}


