package de.hsos.dishdash.db.dao;
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.ArrayList;
import java.util.List;
import de.hsos.dishdash.db.entity.Recipe;

@Dao
public interface RecipeDAO {
    //Create new Recipe into DB
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Recipe recipe);

    //Update Recipe in DB
    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Recipe recipe);

    @Delete
    void delete(Recipe recipe);

    @Query("SELECT * FROM recipe")
    LiveData<List<Recipe>> getAllRecipes();

    @Query("SELECT * FROM recipe WHERE id = :recipeId")
    Recipe getRecipeById(int recipeId);

    @Query("SELECT * FROM recipe WHERE title = :recipeTitle")
    LiveData<Recipe> getRecipeByTitle(String recipeTitle);

    @Query("SELECT * FROM recipe WHERE categoryId = :categoryId")
    LiveData<List<Recipe>> getRecipesByCategoryId(int categoryId);

    @Query("SELECT * FROM recipe WHERE title LIKE '%' || :searchQuery || '%'")
    LiveData<List<Recipe>> getRecipesBySearchQuery(String searchQuery);

    @Query("UPDATE recipe SET bookmarked = NOT bookmarked WHERE id = :recipeId")
    void toggleRecipeBookmark(long recipeId);
}

