package de.hsos.dishdash.db.repository;
import android.app.Application;
import androidx.lifecycle.LiveData;
import java.util.List;
import de.hsos.dishdash.db.repository.DishDashRoomDatabase;
import de.hsos.dishdash.db.dao.CookingStepDAO;
import de.hsos.dishdash.db.entity.CookingStep;

public class CookingStepRepository {
    private CookingStepDAO cookingStepDAO;
    private LiveData<List<CookingStep>> allCookingSteps;

    public CookingStepRepository(Application application) {
        DishDashRoomDatabase database = DishDashRoomDatabase.getInstance(application);
        cookingStepDAO = database.cookingStepDAO();
        allCookingSteps = cookingStepDAO.getAllCookingSteps();
    }

    public LiveData<List<CookingStep>> getAllCookingSteps() {
        return allCookingSteps;
    }

    public void insertCookingStep(CookingStep cookingStep) {
        DishDashRoomDatabase.databaseWriterExecutorService.execute(() -> cookingStepDAO.insert(cookingStep));
    }

    public void updateCookingStep(CookingStep cookingStep) {
        DishDashRoomDatabase.databaseWriterExecutorService.execute(() -> cookingStepDAO.update(cookingStep));
    }

    public void deleteCookingStep(CookingStep cookingStep) {
        DishDashRoomDatabase.databaseWriterExecutorService.execute(() -> cookingStepDAO.delete(cookingStep));
    }

    public CookingStep getCookingStepById(int cookingStepId) {
        return cookingStepDAO.getCookingStepById(cookingStepId);
    }
}

