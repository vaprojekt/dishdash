package de.hsos.dishdash.db.repository;

import android.app.Application;
import androidx.lifecycle.LiveData;
import java.util.List;
import de.hsos.dishdash.db.repository.DishDashRoomDatabase;
import de.hsos.dishdash.db.dao.UserDAO;
import de.hsos.dishdash.db.entity.User;

public class UserRepository {
    private UserDAO userDAO;
    private LiveData<List<User>> allUsers;

    public UserRepository(Application application) {
        DishDashRoomDatabase database = DishDashRoomDatabase.getInstance(application);
        userDAO = database.userDAO();
        allUsers = userDAO.getAllUsers();
    }

    public LiveData<List<User>> getAllUsers() {
        return allUsers;
    }

    public void insertUser(User user) {
        DishDashRoomDatabase.databaseWriterExecutorService.execute(() -> userDAO.insert(user));
    }

    public void updateUser(User user) {
        DishDashRoomDatabase.databaseWriterExecutorService.execute(() -> userDAO.update(user));
    }

    public void deleteUser(User user) {
        DishDashRoomDatabase.databaseWriterExecutorService.execute(() -> userDAO.delete(user));
    }

    public LiveData<User> getUserByUserName(String username) {
        return userDAO.getUserByUserName(username);
    }

    public LiveData<User> getUserByEmail(String email) {
        return userDAO.getUserByEmail(email);
    }
}
