package de.hsos.dishdash.db.converter;

import androidx.room.TypeConverter;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.util.List;

import de.hsos.dishdash.db.entity.Ingredient;
import de.hsos.dishdash.db.entity.Recipe;
import de.hsos.dishdash.db.entity.Rating;
import de.hsos.dishdash.db.entity.User;

public class ListConverter {

    @TypeConverter
    public static List<Recipe> fromRecipeString(String value) {
        return new Gson().fromJson(value, new TypeToken<List<Recipe>>() {}.getType());
    }

    @TypeConverter
    public static String toRecipeString(List<Recipe> recipeList) {
        return new Gson().toJson(recipeList);
    }

    @TypeConverter
    public static List<Rating> fromRatingString(String value) {
        return new Gson().fromJson(value, new TypeToken<List<Rating>>() {}.getType());
    }

    @TypeConverter
    public static String toRatingString(List<Rating> ratingList) {
        return new Gson().toJson(ratingList);
    }

    @TypeConverter
    public static List<Ingredient> fromIngredientString(String value) {
        return new Gson().fromJson(value, new TypeToken<List<Ingredient>>() {}.getType());
    }

    @TypeConverter
    public static String toIngredientString(List<Ingredient> ingredientList) {
        return new Gson().toJson(ingredientList);
    }

/*
    @TypeConverter
    public static List<User> fromUserString(String value) {
        return new Gson().fromJson(value, new TypeToken<List<User>>() {}.getType());
    }

    @TypeConverter
    public static String toUserString(List<User> userList) {
        return new Gson().toJson(userList);
    }*/

}
