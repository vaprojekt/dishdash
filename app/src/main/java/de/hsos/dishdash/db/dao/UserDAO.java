package de.hsos.dishdash.db.dao;
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import java.util.List;
import de.hsos.dishdash.db.entity.User;


@Dao
public interface UserDAO {
    //Create add new User into DB
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(User user);

    //Delete User in DB
    @Delete
    void delete(User user);

    //Update User in DB
    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(User user);

    //List Users in DB
    @Query("SELECT * FROM user")
    LiveData<List<User>> getAllUsers();

    //Get User by their username
    @Query("SELECT * FROM user WHERE username = :userName")
    LiveData<User> getUserByUserName(String userName);

    //Get User by their email
    @Query("SELECT * FROM user WHERE email = :email")
    LiveData<User> getUserByEmail(String email);

}
