package de.hsos.dishdash.db.repository;

import android.app.Application;
import androidx.lifecycle.LiveData;
import java.util.List;
import de.hsos.dishdash.db.repository.DishDashRoomDatabase;
import de.hsos.dishdash.db.dao.IngredientDAO;
import de.hsos.dishdash.db.entity.Ingredient;

public class IngredientRepository {
    private IngredientDAO ingredientDAO;
    private LiveData<List<Ingredient>> allIngredients;

    public IngredientRepository(Application application) {
        DishDashRoomDatabase database = DishDashRoomDatabase.getInstance(application);
        ingredientDAO = database.ingredientDAO();
        allIngredients = ingredientDAO.getAllIngredients();
    }

    public LiveData<List<Ingredient>> getAllIngredients() {
        return allIngredients;
    }

    public void insertIngredient(Ingredient ingredient) {
        DishDashRoomDatabase.databaseWriterExecutorService.execute(() -> ingredientDAO.insert(ingredient));
    }

    public void updateIngredient(Ingredient ingredient) {
        DishDashRoomDatabase.databaseWriterExecutorService.execute(() -> ingredientDAO.update(ingredient));
    }

    public void deleteIngredient(Ingredient ingredient) {
        DishDashRoomDatabase.databaseWriterExecutorService.execute(() -> ingredientDAO.delete(ingredient));
    }

    public Ingredient getIngredientById(int ingredientId) {
        return ingredientDAO.getIngredientById(ingredientId);
    }
}

