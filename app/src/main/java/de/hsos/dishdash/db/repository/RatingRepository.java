package de.hsos.dishdash.db.repository;
import android.app.Application;
import androidx.lifecycle.LiveData;
import java.util.List;
import de.hsos.dishdash.db.repository.DishDashRoomDatabase;
import de.hsos.dishdash.db.dao.RatingDAO;
import de.hsos.dishdash.db.entity.Rating;

public class RatingRepository {
    private RatingDAO ratingDAO;
    private LiveData<List<Rating>> allRatings;

    public RatingRepository(Application application) {
        DishDashRoomDatabase database = DishDashRoomDatabase.getInstance(application);
        ratingDAO = database.ratingDAO();
        allRatings = ratingDAO.getAllRatings();
    }

    public LiveData<List<Rating>> getAllRatings() {
        return allRatings;
    }

    public void insertRating(Rating rating) {
        DishDashRoomDatabase.databaseWriterExecutorService.execute(() -> ratingDAO.insert(rating));
    }

    public void updateRating(Rating rating) {
        DishDashRoomDatabase.databaseWriterExecutorService.execute(() -> ratingDAO.update(rating));
    }

    public void deleteRating(Rating rating) {
        DishDashRoomDatabase.databaseWriterExecutorService.execute(() -> ratingDAO.delete(rating));
    }

    public Rating getRatingById(int ratingId) {
        return ratingDAO.getRatingById(ratingId);
    }
}
