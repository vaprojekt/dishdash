package de.hsos.dishdash.db.repository;
import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import de.hsos.dishdash.db.converter.DateConverter;
import de.hsos.dishdash.db.converter.ListConverter;
import de.hsos.dishdash.db.dao.CategoryDAO;
import de.hsos.dishdash.db.dao.CookingStepDAO;
import de.hsos.dishdash.db.dao.IngredientDAO;
import de.hsos.dishdash.db.dao.RatingDAO;
import de.hsos.dishdash.db.dao.RecipeDAO;
import de.hsos.dishdash.db.dao.UserDAO;
import de.hsos.dishdash.db.entity.Category;
import de.hsos.dishdash.db.entity.CookingStep;
import de.hsos.dishdash.db.entity.Ingredient;
import de.hsos.dishdash.db.entity.Rating;
import de.hsos.dishdash.db.entity.Recipe;
import de.hsos.dishdash.db.entity.User;

@Database(entities={Rating.class, User.class, Recipe.class, Category.class, CookingStep.class, Ingredient.class}, version=1, exportSchema = false)
@TypeConverters({DateConverter.class, ListConverter.class}) // Add PlayerListConverter here
public abstract class DishDashRoomDatabase extends RoomDatabase {

    private static volatile DishDashRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    static ExecutorService databaseWriterExecutorService =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static DishDashRoomDatabase getInstance(final Context context) {
        if (INSTANCE == null) {
            //Bei .class wird das class-Objekt als Monitor verwendet, um das Locking zu realisieren.
            //Da es zur Laufzeit nur genau ein class-Objekt gibt, ist Threadsicherheit fuer
            //den definierten Block gegeben.
            //Auf Ebene von Klassenmethoden ist dies der optimale Schutz.
            //Auf Ebene von Objektmethoden sollte ein Objekt (bspw. this) als Monitor genutzt werden.
            //siehe u.a.: https://howtodoinjava.com/java/multi-threading/object-vs-class-level-locking/
            synchronized (DishDashRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), DishDashRoomDatabase.class, "dishdash_database")
                            //.fallbackToDestructiveMigration() //Update version then uncomment this line
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public abstract RecipeDAO recipeDAO();
    public abstract UserDAO userDAO();
    public abstract RatingDAO ratingDAO();
    public abstract CategoryDAO categoryDAO();
    public abstract IngredientDAO ingredientDAO();
    public abstract CookingStepDAO cookingStepDAO();
}
