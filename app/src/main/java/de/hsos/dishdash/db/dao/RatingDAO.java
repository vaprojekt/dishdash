package de.hsos.dishdash.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import java.util.List;
import de.hsos.dishdash.db.entity.Rating;

@Dao
public interface RatingDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Rating rating);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Rating rating);

    @Delete
    void delete(Rating rating);

    @Query("SELECT * FROM rating")
    LiveData<List<Rating>> getAllRatings();

    @Query("SELECT * FROM rating WHERE id = :ratingId")
    Rating getRatingById(int ratingId);
}
