package de.hsos.dishdash.db.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.ArrayList;
import java.util.List;

@Entity(indices = {@Index(value={"username"}, unique = true)})
public class User {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @NonNull
    private String username;
    private String email;
    private String password;
    private String name;
    private int profilePictureResource; // Resource ID for the profile picture
    private String profilePictureUrl; // URL for the profile picture
    private List<Recipe> favorites;

    public User(@NonNull String username, String email, String password, String name) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.name = name;
        this.profilePictureResource = 0;
        this.profilePictureUrl = "";
        this.favorites = new ArrayList<>();
    }

    // Getters and setters for the fields

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getUsername() {
        return username;
    }

    public void setUsername(@NonNull String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProfilePictureResource() {
        return profilePictureResource;
    }

    public void setProfilePictureResource(int profilePictureResource) {
        this.profilePictureResource = profilePictureResource;
    }

    public String getProfilePictureUrl() {
        return profilePictureUrl;
    }

    public void setProfilePictureUrl(String profilePictureUrl) {
        this.profilePictureUrl = profilePictureUrl;
    }

    public List<Recipe> getFavorites() {
        return favorites;
    }

    public void setFavorites(List<Recipe> favorites) {
        this.favorites = favorites;
    }

}

