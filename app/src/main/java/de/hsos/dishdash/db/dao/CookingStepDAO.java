package de.hsos.dishdash.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import java.util.List;
import de.hsos.dishdash.db.entity.CookingStep;

@Dao
public interface CookingStepDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(CookingStep cookingStep);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(CookingStep cookingStep);

    @Delete
    void delete(CookingStep cookingStep);

    @Query("SELECT * FROM cookingstep")
    LiveData<List<CookingStep>> getAllCookingSteps();

    @Query("SELECT * FROM cookingstep WHERE id = :cookingStepId")
    CookingStep getCookingStepById(int cookingStepId);
}

