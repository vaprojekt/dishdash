package de.hsos.dishdash.db.repository;

import android.app.Application;
import androidx.lifecycle.LiveData;
import java.util.List;
import de.hsos.dishdash.db.repository.DishDashRoomDatabase;
import de.hsos.dishdash.db.dao.CategoryDAO;
import de.hsos.dishdash.db.entity.Category;

public class CategoryRepository {
    private CategoryDAO categoryDAO;
    private LiveData<List<Category>> allCategories;

    public CategoryRepository(Application application) {
        DishDashRoomDatabase database = DishDashRoomDatabase.getInstance(application);
        categoryDAO = database.categoryDAO();
        allCategories = categoryDAO.getAllCategories();
    }

    public LiveData<List<Category>> getAllCategories() {
        return allCategories;
    }

    public void insertCategory(Category category) {
        DishDashRoomDatabase.databaseWriterExecutorService.execute(() -> categoryDAO.insert(category));
    }

    public void updateCategory(Category category) {
        DishDashRoomDatabase.databaseWriterExecutorService.execute(() -> categoryDAO.update(category));
    }

    public void deleteCategory(Category category) {
        DishDashRoomDatabase.databaseWriterExecutorService.execute(() -> categoryDAO.delete(category));
    }

    public Category getCategoryById(int categoryId) {
        return categoryDAO.getCategoryById(categoryId);
    }

    public Category getCategoryByName(String selectedCategoryName) {
        return categoryDAO.getCategoryByName(selectedCategoryName);
    }

    public void deleteCategoryByName(String categoryName) {
        DishDashRoomDatabase.databaseWriterExecutorService.execute(() -> {
            Category category = getCategoryByName(categoryName);
            if (category != null) {
                categoryDAO.delete(category);
            }
        });
    }
}

