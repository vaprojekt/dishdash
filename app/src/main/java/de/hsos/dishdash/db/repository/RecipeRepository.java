package de.hsos.dishdash.db.repository;

import android.app.Application;
import androidx.lifecycle.LiveData;

import java.util.ArrayList;
import java.util.List;

import de.hsos.dishdash.db.dao.RecipeDAO;
import de.hsos.dishdash.db.entity.Recipe;

public class RecipeRepository {
    private RecipeDAO recipeDAO;
    private LiveData<List<Recipe>> allRecipes;

    public RecipeRepository(Application application) {
        DishDashRoomDatabase database = DishDashRoomDatabase.getInstance(application);
        recipeDAO = database.recipeDAO();
        allRecipes = recipeDAO.getAllRecipes();
    }

    public LiveData<List<Recipe>> getAllRecipes() {
        return allRecipes;
    }

    public void insertRecipe(Recipe recipe) {
        DishDashRoomDatabase.databaseWriterExecutorService.execute(() -> recipeDAO.insert(recipe));
    }

    public void updateRecipe(Recipe recipe) {
        DishDashRoomDatabase.databaseWriterExecutorService.execute(() -> recipeDAO.update(recipe));
    }

    public void deleteRecipe(Recipe recipe) {
        DishDashRoomDatabase.databaseWriterExecutorService.execute(() -> recipeDAO.delete(recipe));
    }

    public Recipe getRecipeById(int recipeId) {
        return recipeDAO.getRecipeById(recipeId);
    }

    public LiveData<Recipe> getRecipeByTitle(String recipeTitle) {
        return recipeDAO.getRecipeByTitle(recipeTitle);
    }

    public LiveData<List<Recipe>> getRecipesByCategoryId(int categoryId) {
        return recipeDAO.getRecipesByCategoryId(categoryId);
    }

    public LiveData<List<Recipe>> getRecipesBySearchQuery(String searchQuery) {
        return recipeDAO.getRecipesBySearchQuery(searchQuery);
    }

    public void toggleRecipeBookmark(int recipeId) {
        DishDashRoomDatabase.databaseWriterExecutorService.execute(() -> recipeDAO.toggleRecipeBookmark(recipeId));
    }
}

