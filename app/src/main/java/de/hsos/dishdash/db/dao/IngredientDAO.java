package de.hsos.dishdash.db.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import java.util.List;
import de.hsos.dishdash.db.entity.Ingredient;

@Dao
public interface IngredientDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Ingredient ingredient);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Ingredient ingredient);

    @Delete
    void delete(Ingredient ingredient);

    @Query("SELECT * FROM ingredient")
    LiveData<List<Ingredient>> getAllIngredients();

    @Query("SELECT * FROM ingredient WHERE id = :ingredientId")
    Ingredient getIngredientById(int ingredientId);
}

