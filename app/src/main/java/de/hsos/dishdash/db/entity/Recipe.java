package de.hsos.dishdash.db.entity;
import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import java.util.List;

@Entity(indices = {
        @Index(value = {"title"}, unique = true),
        @Index(value = {"userId"}),
        @Index(value = {"categoryId"})
},
        foreignKeys = {
                @ForeignKey(entity = User.class, parentColumns = "id", childColumns = "userId"),
                @ForeignKey(entity = Category.class, parentColumns = "id", childColumns = "categoryId")
        })
public class Recipe {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @NonNull
    private String title;
    private int cookingTime;
    private int userId;
    private int categoryId;
    private int numOfServe;
    private boolean bookmarked;
    private int recipePictureResource; // Resource ID for the recipe picture
    private String recipePictureUrl; // URL for the recipe picture

    // Getters and setters

    public Recipe(@NonNull String title, int cookingTime, int userId, int categoryId,
                  int numOfServe, boolean bookmarked, int recipePictureResource, String recipePictureUrl) {
        this.title = title;
        this.cookingTime = cookingTime;
        this.userId = userId;
        this.categoryId = categoryId;
        this.numOfServe = numOfServe;
        this.bookmarked = bookmarked;
        this.recipePictureResource = recipePictureResource;
        this.recipePictureUrl = recipePictureUrl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    public void setTitle(@NonNull String title) {
        this.title = title;
    }

    public int getCookingTime() {
        return cookingTime;
    }

    public void setCookingTime(int cookingTime) {
        this.cookingTime = cookingTime;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getNumOfServe() {
        return numOfServe;
    }

    public void setNumOfServe(int numOfServe) {
        this.numOfServe = numOfServe;
    }

    public int getRecipePictureResource() {
        return recipePictureResource;
    }

    public boolean isBookmarked() {
        return bookmarked;
    }

    public void setBookmarked(boolean bookmarked) {
        this.bookmarked = bookmarked;
    }

    public void setRecipePictureResource(int recipePictureResource) {
        this.recipePictureResource = recipePictureResource;
    }

    public String getRecipePictureUrl() {
        return recipePictureUrl;
    }

    public void setRecipePictureUrl(String recipePictureUrl) {
        this.recipePictureUrl = recipePictureUrl;
    }
}

