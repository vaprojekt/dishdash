package de.hsos.dishdash.db.entity;
import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(indices = {@Index(value={"name"}, unique = true)})
public class Category {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @NonNull
    private String name;


    // Constructors, getters, and setters

    public Category(@NonNull String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

}

