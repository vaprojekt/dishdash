package de.hsos.dishdash.db.entity;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(foreignKeys = {
        @ForeignKey(entity = Recipe.class,
                parentColumns = "id",
                childColumns = "recipeId",
                onDelete = ForeignKey.CASCADE)},
        indices = {@Index(value = {"recipeId"})})
public class CookingStep {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private int recipeId;
    private int stepNumber;
    private String description;

    public CookingStep(int recipeId, int stepNumber, String description) {
        this.recipeId = recipeId;
        this.stepNumber = stepNumber;
        this.description = description;
    }

    // Getters and setters for the fields

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(int recipeId) {
        this.recipeId = recipeId;
    }

    public int getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(int stepNumber) {
        this.stepNumber = stepNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
