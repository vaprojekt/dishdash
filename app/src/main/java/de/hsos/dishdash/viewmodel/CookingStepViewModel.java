package de.hsos.dishdash.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import de.hsos.dishdash.db.repository.CookingStepRepository;
import de.hsos.dishdash.db.entity.CookingStep;

public class CookingStepViewModel extends AndroidViewModel {
    private CookingStepRepository cookingStepRepository;
    private LiveData<List<CookingStep>> allCookingSteps;

    public CookingStepViewModel(@NonNull Application application) {
        super(application);
        cookingStepRepository = new CookingStepRepository(application);
        allCookingSteps = cookingStepRepository.getAllCookingSteps();
    }

    public LiveData<List<CookingStep>> getAllCookingSteps() {
        return allCookingSteps;
    }

    public void insert(CookingStep cookingStep) {
        cookingStepRepository.insertCookingStep(cookingStep);
    }

    public void delete(CookingStep cookingStep) {
        cookingStepRepository.deleteCookingStep(cookingStep);
    }

    public void update(CookingStep cookingStep) {
        cookingStepRepository.updateCookingStep(cookingStep);
    }

    public CookingStep getCookingStepById(int cookingStepId) {
        return cookingStepRepository.getCookingStepById(cookingStepId);
    }
}
