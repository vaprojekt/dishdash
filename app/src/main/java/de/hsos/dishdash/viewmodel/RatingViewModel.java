package de.hsos.dishdash.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import de.hsos.dishdash.db.repository.RatingRepository;
import de.hsos.dishdash.db.entity.Rating;

public class RatingViewModel extends AndroidViewModel {
    private RatingRepository ratingRepository;
    private LiveData<List<Rating>> allRatings;

    public RatingViewModel(@NonNull Application application) {
        super(application);
        ratingRepository = new RatingRepository(application);
        allRatings = ratingRepository.getAllRatings();
    }

    public LiveData<List<Rating>> getAllRatings() {
        return allRatings;
    }

    public void insert(Rating rating) {
        ratingRepository.insertRating(rating);
    }

    public void delete(Rating rating) {
        ratingRepository.deleteRating(rating);
    }

    public void update(Rating rating) {
        ratingRepository.updateRating(rating);
    }

    public Rating getRatingById(int ratingId) {
        return ratingRepository.getRatingById(ratingId);
    }

}
