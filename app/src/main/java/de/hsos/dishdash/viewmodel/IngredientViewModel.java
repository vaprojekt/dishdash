package de.hsos.dishdash.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import de.hsos.dishdash.db.repository.IngredientRepository;
import de.hsos.dishdash.db.entity.Ingredient;

public class IngredientViewModel extends AndroidViewModel {
    private IngredientRepository ingredientRepository;
    private LiveData<List<Ingredient>> allIngredients;

    public IngredientViewModel(@NonNull Application application) {
        super(application);
        ingredientRepository = new IngredientRepository(application);
        allIngredients = ingredientRepository.getAllIngredients();
    }

    public LiveData<List<Ingredient>> getAllIngredients() {
        return allIngredients;
    }

    public void insert(Ingredient ingredient) {
        ingredientRepository.insertIngredient(ingredient);
    }

    public void delete(Ingredient ingredient) {
        ingredientRepository.deleteIngredient(ingredient);
    }

    public void update(Ingredient ingredient) {
        ingredientRepository.updateIngredient(ingredient);
    }

    public Ingredient getIngredientById(int ingredientId) {
        return ingredientRepository.getIngredientById(ingredientId);
    }

}
