package de.hsos.dishdash.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import de.hsos.dishdash.db.repository.CategoryRepository;
import de.hsos.dishdash.db.entity.Category;

public class CategoryViewModel extends AndroidViewModel {
    private CategoryRepository categoryRepository;
    private LiveData<List<Category>> allCategories;

    public CategoryViewModel(@NonNull Application application) {
        super(application);
        categoryRepository = new CategoryRepository(application);
        allCategories = categoryRepository.getAllCategories();
    }

    public LiveData<List<Category>> getAllCategories() {
        return allCategories;
    }

    public void insert(Category category) {
        categoryRepository.insertCategory(category);
    }

    public void delete(Category category) {
        categoryRepository.deleteCategory(category);
    }

    public void update(Category category) {
        categoryRepository.updateCategory(category);
    }

    public Category getCategoryById(int categoryId) {
        return categoryRepository.getCategoryById(categoryId);
    }

    public Category getCategoryByName(String selectedCategoryName) {
        return categoryRepository.getCategoryByName(selectedCategoryName);
    }

    public void deleteCategoryByName(String selectedCategoryName) {
        categoryRepository.deleteCategoryByName(selectedCategoryName);
    }

}
