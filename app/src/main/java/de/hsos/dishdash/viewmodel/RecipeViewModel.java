package de.hsos.dishdash.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.ArrayList;
import java.util.List;

import de.hsos.dishdash.db.repository.RecipeRepository;
import de.hsos.dishdash.db.entity.Recipe;

public class RecipeViewModel extends AndroidViewModel {
    private RecipeRepository recipeRepository;
    private LiveData<List<Recipe>> allRecipes;

    public RecipeViewModel(@NonNull Application application) {
        super(application);
        recipeRepository = new RecipeRepository(application);
        allRecipes = recipeRepository.getAllRecipes();
    }

    public LiveData<List<Recipe>> getAllRecipes() {
        return allRecipes;
    }

    public void insert(Recipe recipe) {
        recipeRepository.insertRecipe(recipe);
    }

    public void delete(Recipe recipe) {
        recipeRepository.deleteRecipe(recipe);
    }

    public void update(Recipe recipe) {
        recipeRepository.updateRecipe(recipe);
    }

    public Recipe getRecipeById(int recipeId) {
        return recipeRepository.getRecipeById(recipeId);
    }

    public LiveData<Recipe> getRecipeByTitle(String recipeTitle) {
        return recipeRepository.getRecipeByTitle(recipeTitle);
    }

    public LiveData<List<Recipe>> getRecipesByCategoryId(int categoryId) {
        return recipeRepository.getRecipesByCategoryId(categoryId);
    }

    public LiveData<List<Recipe>> getRecipesBySearchQuery(String searchQuery) {
        return recipeRepository.getRecipesBySearchQuery(searchQuery);
    }

    public void toggleRecipeBookmark(int recipeId) {
        recipeRepository.toggleRecipeBookmark(recipeId);
        Log.d(getClass().getName(),"toggleRecipeBookmark updated for recipeId: " + recipeId);
    }
}
