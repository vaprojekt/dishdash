package de.hsos.dishdash.viewmodel;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import de.hsos.dishdash.db.repository.UserRepository;
import de.hsos.dishdash.db.entity.User;

public class UserViewModel extends AndroidViewModel {
    private UserRepository userRepository;
    private LiveData<List<User>> allUsers;

    public UserViewModel(@NonNull Application application) {
        super(application);
        userRepository = new UserRepository(application);
        allUsers = userRepository.getAllUsers();
    }

    public LiveData<List<User>> getAllUsers() {
        return allUsers;
    }

    public void insert(User user) {
        userRepository.insertUser(user);
        if(getAllUsers().getValue()!=null) Log.d(getClass().getName(), "data added in repository; 1 entry added; total " + getAllUsers().getValue().size() + " entries.");
    }

    public void delete(User user) {
        userRepository.deleteUser(user);
        if(getAllUsers().getValue()!=null) Log.d(getClass().getName(), "data deleted in repository; 1 entry deleted in total " + getAllUsers().getValue().size() + " entries.");
    }

    public void update(User user) {
        userRepository.updateUser(user);
        if(getAllUsers().getValue()!=null) Log.d(getClass().getName(), "data updated in repository; 1 entry updated in total " + getAllUsers().getValue().size() + " entries.");
    }

    public LiveData<User> getUserByUserName(String username) {
        return userRepository.getUserByUserName(username);
    }

    public LiveData<User> getUserByEmail(String email) {
        return userRepository.getUserByEmail(email);
    }
}
