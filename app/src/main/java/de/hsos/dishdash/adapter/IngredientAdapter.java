package de.hsos.dishdash.adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import de.hsos.dishdash.R;
import de.hsos.dishdash.db.entity.Ingredient;

public class IngredientAdapter extends RecyclerView.Adapter<IngredientAdapter.ViewHolder> {

    private List<Ingredient> ingredientsList;

    public IngredientAdapter(List<Ingredient> ingredientsList) {
        this.ingredientsList = ingredientsList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ingredient, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Ingredient ingredient = ingredientsList.get(position);
        holder.editTextIngredientName.setText(ingredient.getName());
        holder.editTextIngredientQuantity.setText(String.valueOf(ingredient.getQuantity()));
        holder.editTextIngredientUnit.setText(ingredient.getUnitOfMeasurement());
        holder.editTextIngredientAdditionalInfo.setText(ingredient.getAdditionalInformation());

        // Save Icon Click Listener
        holder.ivSaveIngredient.setOnClickListener(v -> {
            String name = holder.editTextIngredientName.getText().toString().trim();
            String quantityStr = holder.editTextIngredientQuantity.getText().toString().trim();
            String unit = holder.editTextIngredientUnit.getText().toString().trim();
            String additionalInformation = holder.editTextIngredientAdditionalInfo.getText().toString().trim();

            // Validate inputs before saving
            if (!name.isEmpty() && !quantityStr.isEmpty() && !unit.isEmpty()) {
                double quantity = Double.parseDouble(quantityStr);
                Ingredient updatedIngredient = new Ingredient(name, quantity, unit, additionalInformation, -1);
                ingredientsList.set(position, updatedIngredient);
                // Notify adapter about the change in data
                notifyDataSetChanged();
            }
        });

        // Add Icon Click Listener
        holder.ivAddIngredient.setOnClickListener(v -> {
            ingredientsList.add(position + 1, new Ingredient("", 0, "","",-1));
            notifyDataSetChanged();
        });

        // Remove Icon Click Listener
        holder.ivRemoveIngredient.setOnClickListener(v -> {
            if (ingredientsList.size() > 1) { // Ensure there's at least one ingredient
                ingredientsList.remove(position);
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return ingredientsList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public EditText editTextIngredientName;
        public EditText editTextIngredientQuantity;
        public EditText editTextIngredientUnit;
        public EditText editTextIngredientAdditionalInfo;
        public ImageView ivSaveIngredient;
        public ImageView ivAddIngredient;
        public ImageView ivRemoveIngredient;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            editTextIngredientName = itemView.findViewById(R.id.editTextIngredientName);
            editTextIngredientQuantity = itemView.findViewById(R.id.editTextIngredientQuantity);
            editTextIngredientUnit = itemView.findViewById(R.id.editTextIngredientUnit);
            editTextIngredientAdditionalInfo = itemView.findViewById(R.id.editTextIngredientAdditionalInfo);
            ivSaveIngredient = itemView.findViewById(R.id.ivSaveIngredient);
            ivAddIngredient = itemView.findViewById(R.id.ivAddIngredient);
            ivRemoveIngredient = itemView.findViewById(R.id.ivRemoveIngredient);
        }
    }
}
