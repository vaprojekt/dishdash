package de.hsos.dishdash.adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import de.hsos.dishdash.R;
import de.hsos.dishdash.db.entity.Recipe;

public class SearchResultsAdapter extends RecyclerView.Adapter<SearchResultsAdapter.SearchResultViewHolder> {

    private ArrayList<Recipe> searchResults = new ArrayList<>();
    private RecipeClickListener recipeClickListener;
    private RecipeBookmarkClickListener recipeBookmarkClickListener;

    // Constructor to pass click listeners
    public SearchResultsAdapter(RecipeClickListener recipeClickListener, RecipeBookmarkClickListener recipeBookmarkClickListener) {
        this.recipeClickListener = recipeClickListener;
        this.recipeBookmarkClickListener = recipeBookmarkClickListener;
    }

    public void setData(List<Recipe> recipeList) {
        searchResults.clear();
        searchResults.addAll(recipeList);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SearchResultViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_result, parent, false);
        return new SearchResultViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull SearchResultViewHolder holder, int position) {
        Recipe recipe = searchResults.get(position);
        holder.tvRecipeName.setText(recipe.getTitle());

        // Set the recipe image using an image loading library like Glide or Picasso
        //Glide.with(holder.itemView.getContext()).load(recipe.getRecipePictureUrl()).into(holder.ivRecipeImage);

        updateFavorite(holder,recipe);
        holder.tvCookingTime.setText(String.format(holder.itemView.getContext().getString(R.string.search_result_duration_format), recipe.getCookingTime()));

        // Add click listeners for favorite icon and the whole item
        holder.btnFavorite.setOnClickListener(v -> {
            if (recipeBookmarkClickListener != null) {
                recipeBookmarkClickListener.onRecipeBookmarkClick(position);
            }
        });

        holder.itemView.setOnClickListener(v -> {
            if (recipeClickListener != null) {
                recipeClickListener.onRecipeClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return searchResults.size();
    }

    // Helper method to get the recipe at a specific position
    public Recipe getRecipeAtPosition(int position) {
        if (position >= 0 && position < searchResults.size()) {
            return searchResults.get(position);
        }
        return null;
    }

    // Click listener interface for the recipe item click
    public interface RecipeClickListener {
        void onRecipeClick(int position);
    }

    // Click listener interface for the recipe bookmark icon click
    public interface RecipeBookmarkClickListener {
        void onRecipeBookmarkClick(int position);
    }

    private void updateFavorite(@NonNull SearchResultViewHolder holder, Recipe recipe) {
        // Set the favorite icon based on the bookmarked status of the recipe
        if (recipe.isBookmarked()) {
            holder.btnFavorite.setImageResource(R.drawable.ic_baseline_favorite_24);
        } else {
            holder.btnFavorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
        }
    }

    static class SearchResultViewHolder extends RecyclerView.ViewHolder {
        private TextView tvRecipeName;
        private ImageView ivRecipeImage;
        private ImageView btnFavorite;
        private TextView tvCookingTime;
        private CardView cvSearchRecipe;

        public SearchResultViewHolder(View itemView) {
            super(itemView);
            tvRecipeName = itemView.findViewById(R.id.tvRecipeName);
            ivRecipeImage = itemView.findViewById(R.id.ivRecipeImage);
            btnFavorite = itemView.findViewById(R.id.btnFavorite);
            tvCookingTime = itemView.findViewById(R.id.tvCookingTime);
            cvSearchRecipe = itemView.findViewById(R.id.cvSearchRecipe);

        }
    }
}