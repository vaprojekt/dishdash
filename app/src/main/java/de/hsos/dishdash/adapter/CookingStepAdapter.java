package de.hsos.dishdash.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;
import de.hsos.dishdash.R;
import de.hsos.dishdash.db.entity.CookingStep;

public class CookingStepAdapter extends RecyclerView.Adapter<CookingStepAdapter.CookingStepViewHolder> {

    private List<CookingStep> cookingStepsList;

    public CookingStepAdapter(List<CookingStep> cookingStepsList) {
        this.cookingStepsList = cookingStepsList;
    }

    @NonNull
    @Override
    public CookingStepViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_cooking_step, parent, false);
        return new CookingStepViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CookingStepViewHolder holder, int position) {
        CookingStep cookingStep = cookingStepsList.get(position);
        holder.editTextStepNumber.setText(String.valueOf(cookingStep.getStepNumber()));
        holder.editTextCookingStep.setText(cookingStep.getDescription());

        // Save Icon Click Listener
        holder.ivSaveStep.setOnClickListener(v -> {
            String stepNumberStr = holder.editTextStepNumber.getText().toString().trim();
            String description = holder.editTextCookingStep.getText().toString().trim();

            // Validate inputs before saving
            if (!stepNumberStr.isEmpty() && !description.isEmpty()) {
                int stepNumber = Integer.parseInt(stepNumberStr);
                CookingStep updatedStep = new CookingStep(-1,stepNumber, description);
                cookingStepsList.set(position, updatedStep);
                // Notify adapter about the change in data
                notifyDataSetChanged();
            }
        });

        // Add Icon Click Listener
        holder.ivAddStep.setOnClickListener(v -> {
            cookingStepsList.add(position + 1, new CookingStep(-1, this.getItemCount()+1,""));
            notifyDataSetChanged();
        });

        // Remove Icon Click Listener
        holder.ivRemoveStep.setOnClickListener(v -> {
            if (cookingStepsList.size() > 1) { // Ensure there's at least one step
                cookingStepsList.remove(position);
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return cookingStepsList.size();
    }

    public static class CookingStepViewHolder extends RecyclerView.ViewHolder {

        public EditText editTextStepNumber;
        public EditText editTextCookingStep;
        public ImageView ivSaveStep;
        public ImageView ivAddStep;
        public ImageView ivRemoveStep;

        public CookingStepViewHolder(@NonNull View itemView) {
            super(itemView);
            editTextStepNumber = itemView.findViewById(R.id.editTextStepNumber);
            editTextCookingStep = itemView.findViewById(R.id.editTextCookingStep);
            ivSaveStep = itemView.findViewById(R.id.ivSaveStep);
            ivAddStep = itemView.findViewById(R.id.ivAddStep);
            ivRemoveStep = itemView.findViewById(R.id.ivRemoveStep);
        }
    }
}

