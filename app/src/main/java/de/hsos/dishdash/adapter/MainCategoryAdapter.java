package de.hsos.dishdash.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import de.hsos.dishdash.R;
import de.hsos.dishdash.db.entity.Recipe;

public class MainCategoryAdapter extends RecyclerView.Adapter<MainCategoryAdapter.RecipeViewHolder> {

    private ArrayList<Recipe> arrMainCategory = new ArrayList<>();
    private RecipeClickListener clickListener;

    public void setClickListener(RecipeClickListener clickListener) {
        this.clickListener = clickListener;
    }


    static class RecipeViewHolder extends RecyclerView.ViewHolder {
        private TextView recipeName;
        private ImageView ivAvatar;
        private ImageView ivFavorite;
        private TextView tvStarNumber;
        private TextView tvDuration;
        private ImageView img_dish;
        private CardView cvRecipe;

        public RecipeViewHolder(View view) {
            super(view);

            recipeName = itemView.findViewById(R.id.tvDishName);
            ivAvatar = itemView.findViewById(R.id.ivAvatar);
            tvStarNumber = itemView.findViewById(R.id.tvStarNumber);
            ivFavorite = itemView.findViewById(R.id.ivFavorite);
            tvDuration = itemView.findViewById(R.id.tvDuration);
            img_dish = itemView.findViewById(R.id.img_dish);
            cvRecipe = itemView.findViewById(R.id.cvRecipe);
        }
    }

    public void setData(List<Recipe> arrData) {
        arrMainCategory.clear();
        arrMainCategory.addAll(arrData);
        notifyDataSetChanged();
    }

    @Override
    public RecipeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rv_main_category, parent, false);
        return new RecipeViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return arrMainCategory.size();
    }

    @Override
    public void onBindViewHolder(RecipeViewHolder holder, int position) {
        Recipe recipe = arrMainCategory.get(position);

        holder.recipeName.setText(recipe.getTitle());

        //ivAvatar, tvStarNumber
        updateFavorite(holder,recipe);
        //Use resource string with placeholders.
        holder.tvDuration.setText(String.format(holder.itemView.getContext().getString(R.string.duration_format), recipe.getCookingTime()));

        //img_dish

        // Set click listener for the recipe item (cvRecipe)
        holder.cvRecipe.setOnClickListener(v -> {
            if (clickListener != null) {
                clickListener.onRecipeClick(recipe, position);
            }
        });

    }

    private void updateFavorite(RecipeViewHolder holder, Recipe recipe) {
        // Show the bookmarked icon if the note is bookmarked
        if (recipe.isBookmarked()) {
            holder.ivFavorite.setImageResource(R.drawable.ic_baseline_favorite_24);
        } else {
            holder.ivFavorite.setImageResource(R.drawable.ic_baseline_favorite_border_24);
        }
    }

    public interface RecipeClickListener {
        void onRecipeClick(Recipe recipe, int position);
    }

}
