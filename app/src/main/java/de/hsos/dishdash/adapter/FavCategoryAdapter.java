package de.hsos.dishdash.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import de.hsos.dishdash.R;
import de.hsos.dishdash.db.entity.Recipe;

public class FavCategoryAdapter extends RecyclerView.Adapter<FavCategoryAdapter.RecipeViewHolder> {

    private ArrayList<Recipe> arrFavCategory = new ArrayList<>();

    static class RecipeViewHolder extends RecyclerView.ViewHolder {
        private TextView recipeName;
        private ImageView ivAvatar;
        private ImageView menuIcon;

        public RecipeViewHolder(View view) {
            super(view);

            recipeName = itemView.findViewById(R.id.tvDishName);
            //ivAvatar = itemView.findViewById(R.id.ivAvatar);
            //menuIcon = itemView.findViewById(R.id.menu_icon);

        }
    }

    public void setData(List<Recipe> arrData) {
        arrFavCategory.clear();
        arrFavCategory.addAll(arrData);
    }

    @Override
    public RecipeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_rv_fav_category, parent, false);
        return new RecipeViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return arrFavCategory.size();
    }

    @Override
    public void onBindViewHolder(RecipeViewHolder holder, int position) {
        Recipe recipe = arrFavCategory.get(position);
        holder.recipeName.setText(recipe.getTitle());

        // holder.itemView.tvDishName.setText(recipe.getTitle());
    }
}
