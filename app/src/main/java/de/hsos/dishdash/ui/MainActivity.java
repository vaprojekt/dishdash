package de.hsos.dishdash.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;

import de.hsos.dishdash.R;
import de.hsos.dishdash.adapter.FavCategoryAdapter;
import de.hsos.dishdash.adapter.MainCategoryAdapter;
import de.hsos.dishdash.db.entity.Recipe;
import de.hsos.dishdash.viewmodel.CategoryViewModel;
import de.hsos.dishdash.viewmodel.RecipeViewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity implements MainCategoryAdapter.RecipeClickListener {
    private static final String TAG = MainActivity.class.getName();

    //ArrayList<Recipe> arrMainCategory = new ArrayList<>();
    ArrayList<Recipe> arrSubCategory = new ArrayList<>();
    MainCategoryAdapter mainCategoryAdapter;
    FavCategoryAdapter favCategoryAdapter;

    private TextView btnAll, btnCatalog1, btnCatalog2, btnCatalog3, btnCatalog4;
    // Add other views and variables as needed
    private HashMap<TextView, Integer> categoryMap = new HashMap<>();

    private RecipeViewModel recipeViewModel;
    private int userId;
    private int currentCategoryId = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get the intent from LoginActivity
        Intent loginIntent = getIntent();
        userId = loginIntent.getIntExtra("USER_ID", -1);
        Log.d(TAG,"onCreate getIntent UserId: " + userId);

        mainCategoryAdapter = new MainCategoryAdapter();

        favCategoryAdapter = new FavCategoryAdapter();

        recipeViewModel = new ViewModelProvider(this).get(RecipeViewModel.class);

        // Load list of categories from the database
        loadCategoriesFromDatabase(); // This method should fetch the list of categories from the database
        setBtnCatalogLayout();

        handleClickOnFloatingButton(userId); // Pass the userId to the method

        loadAllRecipes();

        // Set up the search query functionality
        setupSearchQuery();

    }

    @Override
    protected void onResume() {
        super.onResume();
        //Reload data when the activity is resumed
        Log.d(TAG,"onResume currentCategoryId: " + currentCategoryId);
        //Default category is All
        if(currentCategoryId==-1) {
            //Reload updated data
            loadAllRecipes();
        }
        else {
            loadRecipesByCategoryId(currentCategoryId);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,"onPause ");
        // Remove all observers when the activity is paused
        removeObserver(currentCategoryId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"onDestroy ");
        // Remove all observers when the activity is destroyed
        removeObserver(currentCategoryId);
    }

    private void removeObserver(int categoryId) {
        recipeViewModel.getAllRecipes().removeObservers(this);
        if(categoryId!=-1) {
            recipeViewModel.getRecipesByCategoryId(categoryId).removeObservers(this);
        }
        Log.d(TAG,"removed Observer categoryId " + categoryId);
    }

    private void setupSearchQuery() {
        SearchView searchView = findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Handle the search query submission
                performSearch(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                // Handle the search query text change (if needed)
                return false;
            }
        });
    }

    private void performSearch(String query) {
        // Start the SearchRecipeActivity and pass the search query as an intent extra
        Intent searchIntent = new Intent(MainActivity.this, SearchRecipeActivity.class);
        searchIntent.putExtra("SEARCH_QUERY", query);
        searchIntent.putExtra("USER_ID", userId); // Pass the userId as an intent extra
        startActivity(searchIntent);
    }

    @Override
    public void onRecipeClick(Recipe recipe, int position) {
        // Handle the click event when a recipe is clicked
        // Start the EditRecipeActivity with the recipe data
        Intent intent = new Intent(MainActivity.this, ShowRecipeActivity.class);
        // Pass the recipe data to the EditRecipeActivity using intent extras
        // For example:
        intent.putExtra("RECIPE_ID", recipe.getId());
        startActivity(intent);
    }

    private void handleClickOnFloatingButton(int userId) { // Receive the userId as a parameter
        // Set click listener for FAB (Floating Action Button)
        FloatingActionButton fabCreateRecipe = findViewById(R.id.fabCreateRecipe);
        fabCreateRecipe.setOnClickListener(v -> {
            // Handle click on FAB to create a new recipe
            openCreateRecipeActivity(userId); // Pass the userId to the method
        });
    }

    private void openCreateRecipeActivity(int userId) {
        // Implement the code to launch the activity for creating a new recipe
        Intent intent = new Intent(MainActivity.this, CreateRecipeActivity.class);
        intent.putExtra("USER_ID", userId); // Pass the userId through the intent
        startActivity(intent);
    }

    private void setDefaultCalog() {
        // Initialize views
        btnAll = findViewById(R.id.btnAll);
        btnCatalog1 = findViewById(R.id.btnCatalog1);
        btnCatalog2 = findViewById(R.id.btnCatalog2);
        btnCatalog3 = findViewById(R.id.btnCatalog3);
        btnCatalog4 = findViewById(R.id.btnCatalog4);

        // Set the default catalog (All) background
        btnAll.setBackgroundResource(R.drawable.btn_bg);
    }

    private void setBtnCatalogLayout() {
        setDefaultCalog();

        // Set click listeners for catalogs
        btnAll.setOnClickListener(v -> {
            // Handle click for All catalog
            // Set background for clicked catalog
            setCatalogBackground(v);
            // Update the RecyclerView to show all recipes
            // TODO: Update RecyclerView for All catalog
            currentCategoryId = -1;
            loadAllRecipes();
        });

        btnCatalog1.setOnClickListener(v -> {
            // Handle click for Catalog 1
            setCatalogBackground(v);
            // Update the RecyclerView to show recipes for Catalog 1
            handleClickOnBtnCategory(v);
        });

        btnCatalog2.setOnClickListener(v -> {
            // Handle click for Catalog 2
            setCatalogBackground(v);
            // Update the RecyclerView to show recipes for Catalog 2
            handleClickOnBtnCategory(v);
        });

        btnCatalog3.setOnClickListener(v -> {
            // Handle click for Catalog 3
            setCatalogBackground(v);
            // Update the RecyclerView to show recipes for Catalog 3
            handleClickOnBtnCategory(v);
        });

        btnCatalog4.setOnClickListener(v -> {
            // Handle click for Catalog 4
            setCatalogBackground(v);
            // Update the RecyclerView to show recipes for Catalog 4
            handleClickOnBtnCategory(v);
        });
    }

    private void handleClickOnBtnCategory(View v) {
        Integer categoryId = categoryMap.get(v);
        if (categoryId != null) {
            currentCategoryId = categoryId;
            loadRecipesByCategoryId(categoryId);
        }
    }

    private void loadRecipesByCategoryId(int categoryId) {
        LiveData<List<Recipe>> recipesLiveData = recipeViewModel.getRecipesByCategoryId(categoryId);

        recipesLiveData.observe(this, new Observer<List<Recipe>>() {
            @Override
            public void onChanged(List<Recipe> recipes) {
                Log.d(TAG,"in loadRecipesByCategoryId recipes size: " + recipes.size());
                // Update the RecyclerView with the new list of recipes
                mainCategoryAdapter.setData(recipes);
                setUpLayOut();
                //recipesLiveData.removeObserver(this); // Remove the observer after receiving the data
            }
        });

    }

    private void loadAllRecipes() {
        LiveData<List<Recipe>> recipesLiveData = recipeViewModel.getAllRecipes();

        recipesLiveData.observe(this, new Observer<List<Recipe>>() {
            @Override
            public void onChanged(List<Recipe> recipes) {
                Log.d(TAG,"in loadAllRecipes recipes size: " + recipes.size());
                // Update the RecyclerView with the new list of recipes
                mainCategoryAdapter.setData(recipes);
                setUpLayOut();
                //recipesLiveData.removeObserver(this); // Remove the observer after receiving the data
            }
        });
    }

    private void setCatalogBackground(View selectedCatalogView) {
        // Reset backgrounds for all catalogs
        btnAll.setBackgroundResource(0);
        btnCatalog1.setBackgroundResource(0);
        btnCatalog2.setBackgroundResource(0);
        btnCatalog3.setBackgroundResource(0);
        btnCatalog4.setBackgroundResource(0);

        // Set background for the selected catalog
        selectedCatalogView.setBackgroundResource(R.drawable.btn_bg);
    }

    private void loadCategoriesFromDatabase() {
        CategoryViewModel categoryViewModel = new ViewModelProvider(this).get(CategoryViewModel.class);

        categoryViewModel.getAllCategories().observe(this, categories -> {
            Log.d(TAG,"in loadCategoriesFromDatabase categories size: " + categories.size());

            if (categories == null || categories.isEmpty()) {
                // Handle the case when there are no categories in the database
                btnCatalog1.setVisibility(View.INVISIBLE);
                btnCatalog2.setVisibility(View.INVISIBLE);
                btnCatalog3.setVisibility(View.INVISIBLE);
                btnCatalog4.setVisibility(View.INVISIBLE);
                return;
            }

            // Determine the maximum number of categories to display
            int maxCategories = Math.min(categories.size(), 4);

            // Show the first 'maxCategories' category TextViews and set their names
            for (int i = 0; i < maxCategories; i++) {
                TextView categoryTextView = null;
                switch (i) {
                    case 0:
                        categoryTextView = btnCatalog1;
                        break;
                    case 1:
                        categoryTextView = btnCatalog2;
                        break;
                    case 2:
                        categoryTextView = btnCatalog3;
                        break;
                    case 3:
                        categoryTextView = btnCatalog4;
                        break;

                }

                if (categoryTextView != null) {
                    categoryTextView.setVisibility(View.VISIBLE);
                    categoryTextView.setText(categories.get(i).getName());

                    // Store the category ID in the categoryMap
                    categoryMap.put(categoryTextView, categories.get(i).getId());
                }
            }

            // Hide the unused category TextViews
            for (int i = maxCategories; i < 4; i++) {
                TextView categoryTextView = null;
                switch (i) {
                    case 0:
                        categoryTextView = btnCatalog1;
                        break;
                    case 1:
                        categoryTextView = btnCatalog2;
                        break;
                    case 2:
                        categoryTextView = btnCatalog3;
                        break;
                    case 3:
                        categoryTextView = btnCatalog4;
                        break;
                }

                if (categoryTextView != null) {
                    categoryTextView.setVisibility(View.INVISIBLE);
                }
            }
        });
    }


    // Other methods and logic for RecyclerView, Floating Action Button, etc.

    private void setUpLayOut() {
        //temporary data
/*        arrMainCategory.add(new Recipe("Beep",0,0,0,0,false,0,""));
        arrMainCategory.add(new Recipe("Chicken",0,0,0,0,false,0,""));
        arrMainCategory.add(new Recipe("Dessert",0,0,0,0,false,0,""));
        arrMainCategory.add(new Recipe("Lamb",0,0,0,0,false,0,""));

        mainCategoryAdapter.setData(arrMainCategory);*/

        // Set the click listener for the MainCategoryAdapter
        mainCategoryAdapter.setClickListener(this);

        arrSubCategory.add(new Recipe("Beef and mustard pie",0,0,0,0,false,0,""));
        arrSubCategory.add(new Recipe("Chicken and mushroom hotpot",0,0,0,0,false,0,""));
        arrSubCategory.add(new Recipe("Banana pancakes",0,0,0,0,false,0,""));
        arrSubCategory.add(new Recipe("Kapsalon",0,0,0,0,false,0,""));

        favCategoryAdapter.setData(arrSubCategory);

        RecyclerView rvMainCategory = findViewById(R.id.rvSubCategory);
        rvMainCategory.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvMainCategory.setAdapter(mainCategoryAdapter);

        RecyclerView rvSubCategory = findViewById(R.id.rvFavoriteRecipes);
        rvSubCategory.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rvSubCategory.setAdapter(favCategoryAdapter);
    }


}
