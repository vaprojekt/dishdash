package de.hsos.dishdash.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import de.hsos.dishdash.R;

public class ShowRecipeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_recipe);
    }
}