package de.hsos.dishdash.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import de.hsos.dishdash.R;
import de.hsos.dishdash.db.entity.User;
import de.hsos.dishdash.viewmodel.UserViewModel;

public class RegisterActivity extends AppCompatActivity {

    public final static String USER_NAME =
            "de.hso.prog3.ss2023.dishdash.RegisterActivity.USER_NAME";
    private static final String TAG = RegisterActivity.class.getName();
    private Button btnRegister;
    private EditText inputName;
    private EditText inputUsername;
    private EditText inputPassword;
    private EditText inputConfirmPassword;
    private EditText inputEmail;
    private TextView tvLogin;

    private UserViewModel userViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setTitle("Register");

        this.userViewModel = new ViewModelProvider(this).get(UserViewModel.class);
        //userViewModel = new UserViewModel(getApplication());

        // Initialize views
        inputName = findViewById(R.id.inputName);
        inputUsername = findViewById(R.id.inputUsername);
        inputConfirmPassword = findViewById(R.id.inputConfirmPassword);
        inputPassword = findViewById(R.id.inputPassword);
        inputEmail = findViewById(R.id.inputEmail);
        btnRegister = findViewById(R.id.btnRegister);
        tvLogin = findViewById(R.id.tvLogin);

        // Handle click on Register button
        btnRegister.setOnClickListener(v -> registerUser());

        // Handle click on Login text view
        tvLogin.setOnClickListener(v -> startActivity(new Intent(RegisterActivity.this, LoginActivity.class)));
    }

    private void registerUser() {
        String name = inputName.getText().toString().trim();
        String username = inputUsername.getText().toString().trim();
        String password = inputPassword.getText().toString().trim();
        String confirmPassword = inputConfirmPassword.getText().toString().trim();
        String email = inputEmail.getText().toString().trim();

        if (name.isEmpty() || username.isEmpty() || password.isEmpty()  || confirmPassword.isEmpty()|| email.isEmpty()) {
            Toast.makeText(RegisterActivity.this, "Please fill in all fields", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!password.equals(confirmPassword)) {
            Toast.makeText(RegisterActivity.this, "Passwords do not match.", Toast.LENGTH_SHORT).show();
            return;
        }

        LiveData<User> existingUser = userViewModel.getUserByUserName(username);
        existingUser.observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                if (user != null) {
                    Toast.makeText(RegisterActivity.this, "Username already exists", Toast.LENGTH_SHORT).show();
                } else {
                    existingUser.removeObserver(this); // Remove the observer after receiving the data
                    LiveData<User> existingUserByEmail = userViewModel.getUserByEmail(email);
                    existingUserByEmail.observe(RegisterActivity.this, new Observer<User>() {
                        @Override
                        public void onChanged(User userByEmail) {
                            if (userByEmail != null) {
                                Toast.makeText(RegisterActivity.this, "User with this email already exists", Toast.LENGTH_SHORT).show();
                            } else {
                                existingUserByEmail.removeObserver(this); // Remove the observer after receiving the data
                                User newUser = new User(username, email, password, name);
                                userViewModel.insert(newUser);
                                Toast.makeText(RegisterActivity.this, "Registration successful", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        }
                    });
                }
            }
        });

    }
}
