package de.hsos.dishdash.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import de.hsos.dishdash.R;

public class EditRecipeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_recipe);
    }
}