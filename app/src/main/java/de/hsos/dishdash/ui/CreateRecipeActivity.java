package de.hsos.dishdash.ui;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import de.hsos.dishdash.R;
import de.hsos.dishdash.adapter.CookingStepAdapter;
import de.hsos.dishdash.adapter.IngredientAdapter;
import de.hsos.dishdash.db.entity.Category;
import de.hsos.dishdash.db.entity.CookingStep;
import de.hsos.dishdash.db.entity.Ingredient;
import de.hsos.dishdash.db.entity.Recipe;
import de.hsos.dishdash.db.entity.User;
import de.hsos.dishdash.viewmodel.CategoryViewModel;
import de.hsos.dishdash.viewmodel.CookingStepViewModel;
import de.hsos.dishdash.viewmodel.IngredientViewModel;
import de.hsos.dishdash.viewmodel.RecipeViewModel;

public class CreateRecipeActivity extends AppCompatActivity {
    private static final String TAG = CreateRecipeActivity.class.getName();
    private CategoryViewModel categoryViewModel;
    private Spinner spinnerCategory;
    private ArrayAdapter<String> categoryAdapter;

    private RecyclerView recyclerViewIngredients;
    private IngredientAdapter ingredientAdapter;
    private List<Ingredient> ingredientsList = new ArrayList<>();

    private RecyclerView recyclerViewSteps;
    private CookingStepAdapter cookingStepAdapter;
    private List<CookingStep> cookingStepsList = new ArrayList<>();


    private RecipeViewModel recipeViewModel;
    private CookingStepViewModel cookingStepViewModel;
    private IngredientViewModel ingredientViewModel;

    private HashMap<String, Category> categoryMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_recipe);

        initViewIngredientAndCookingStep();
        loadCategories();
        updateCategories();
        handleSaveRecipe();
        handleCancelRecipe(); // Add this line to set the click listener for the "Cancel" button

    }

    private void handleCancelRecipe() {
        Button btnCancelRecipe = findViewById(R.id.btnCancelRecipe);
        btnCancelRecipe.setOnClickListener(v -> {
            finish(); // Close the activity without saving the recipe
        });
    }

    private void handleSaveRecipe() {
        recipeViewModel = new ViewModelProvider(this).get(RecipeViewModel.class);
        cookingStepViewModel = new ViewModelProvider(this).get(CookingStepViewModel.class);
        ingredientViewModel = new ViewModelProvider(this).get(IngredientViewModel.class);
        Button btnSaveRecipe = findViewById(R.id.btnSaveRecipe);
        btnSaveRecipe.setOnClickListener(v -> saveRecipe());
    }

    private void saveRecipe() {
        String recipeTitle = ((EditText) findViewById(R.id.editTextRecipeTitle)).getText().toString().trim();
        String recipeCookingTimeStr = ((EditText) findViewById(R.id.editTextCookingTime)).getText().toString().trim();
        String recipeNumOfServeStr = ((EditText) findViewById(R.id.editTextNumOfServe)).getText().toString().trim();
        String recipePictureUrl = ((EditText) findViewById(R.id.editTextRecipePictureUrl)).getText().toString().trim();

        // Check if the recipe title is not empty and a category is selected
        if (TextUtils.isEmpty(recipeTitle)) {
            Toast.makeText(this, "Please enter a recipe title.", Toast.LENGTH_SHORT).show();
            return;
        }

        String selectedCategoryName = spinnerCategory.getSelectedItem().toString();
        if (selectedCategoryName.equals("Loading categories...") || TextUtils.isEmpty(selectedCategoryName)) {
            Toast.makeText(this, "Please choose a category.", Toast.LENGTH_SHORT).show();
            return;
        }

        // Parse cooking time and number of servings
        int cookingTime = 0;
        int numOfServings = 0;
        if (!TextUtils.isEmpty(recipeCookingTimeStr)) {
            cookingTime = Integer.parseInt(recipeCookingTimeStr);
        }
        if (!TextUtils.isEmpty(recipeNumOfServeStr)) {
            numOfServings = Integer.parseInt(recipeNumOfServeStr);
        }

        // Get the selected category name from the spinner
        String selectedCatName = spinnerCategory.getSelectedItem().toString();
        // Get the selected Category object from the categoryMap
        Category selectedCategory = categoryMap.get(selectedCatName);
        // Use the selectedCategory as needed

        // Get the userId from the intent
        int userId = getIntent().getIntExtra("USER_ID", -1);
        Log.d(TAG,"in saveRecipe userId: " + userId);

        // Create a new Recipe object and insert it into the database
        if (selectedCategory != null) {

            //Need to edit to get ingredientsList from adapter
            Log.d(TAG,"in saveRecipe ingredientsList size: " + ingredientsList.size());
            //Get data from Intent from login activity forward user data to this activity

            Recipe newRecipe = new Recipe(
                    recipeTitle,
                    cookingTime,
                    userId, // userId can be added here, if available
                    selectedCategory.getId(),
                    numOfServings,
                    false, // Default not bookmarked
                    R.drawable.default_recipe_picture, // Default picture resource
                    recipePictureUrl
            );
            recipeViewModel.insert(newRecipe);
            LiveData<Recipe> recipeLiveData = recipeViewModel.getRecipeByTitle(recipeTitle);
            recipeLiveData.observe(this, new Observer<Recipe>() {
                @Override
                public void onChanged(Recipe recipe) {
                    if (recipe != null) {
                        Log.d(TAG,"OnClickListener recipe: " + recipe.getId());

                        // Update the recipeId for the created cooking steps and insert them into the database
                        for (CookingStep cookingStep : cookingStepsList) {
                            cookingStep.setRecipeId(recipe.getId());
                            cookingStepViewModel.insert(cookingStep);
                        }

                        // Update the recipeId for the created ingredients and insert them into the database
                        for (Ingredient ingredient : ingredientsList) {
                            ingredient.setRecipeId(recipe.getId());
                            ingredientViewModel.insert(ingredient);
                        }

                    } /*else {
                        Toast.makeText(CreateRecipeActivity.this, "No Recipe Found", Toast.LENGTH_SHORT).show();
                    }*/
                    //recipeLiveData.removeObserver(this); // Remove the observer after receiving the data
                }
            });


            Toast.makeText(this, "Recipe saved successfully.", Toast.LENGTH_SHORT).show();
            finish(); // Finish the activity after saving the recipe
        } else {
            Toast.makeText(this, "Error: Selected category not found.", Toast.LENGTH_SHORT).show();
        }
    }


    //Initialize RecyclerView and its adapters for ingredients and cooking steps
    private void initViewIngredientAndCookingStep() {

        // Default Ingredient
        Ingredient defaultIngredient = new Ingredient("", 0, "", "", -1);
        ingredientsList.add(defaultIngredient);
        // Default CookingStep
        CookingStep defaultCookingStep = new CookingStep(-1, 1, "");
        cookingStepsList.add(defaultCookingStep);

        // Initialize RecyclerView and its adapters for ingredients
        recyclerViewIngredients = findViewById(R.id.recyclerViewIngredients);
        ingredientAdapter = new IngredientAdapter(ingredientsList);
        recyclerViewIngredients.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewIngredients.setAdapter(ingredientAdapter);

        // Initialize RecyclerView and its adapter for cooking steps
        recyclerViewSteps = findViewById(R.id.recyclerViewSteps);
        cookingStepAdapter = new CookingStepAdapter(cookingStepsList);
        recyclerViewSteps.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewSteps.setAdapter(cookingStepAdapter);

        Log.d(TAG,"initview ingredientsList size: " + ingredientsList.size());
    }

    private void updateCategories() {
        ImageView ivAddCategory = findViewById(R.id.ivAddCategory);
        ImageView ivRemoveCategory = findViewById(R.id.ivRemoveCategory);

        ivAddCategory.setOnClickListener(v -> showAddCategoryDialog());
        ivRemoveCategory.setOnClickListener(v -> showRemoveCategoryDialog());

    }

    private void showAddCategoryDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("New Category");
        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setPositiveButton("OK", (dialog, which) -> {
            String categoryName = input.getText().toString().trim();
            if (!categoryName.isEmpty()) {
                Category newCategory = new Category(categoryName);
                categoryViewModel.insert(newCategory);
            }
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        builder.show();
    }

    private void showRemoveCategoryDialog() {
        List<Category> categories = categoryViewModel.getAllCategories().getValue();
        if (categories == null || categories.isEmpty()) {
            Toast.makeText(this, "No categories to remove.", Toast.LENGTH_SHORT).show();
            return;
        }

        String selectedCategoryName = spinnerCategory.getSelectedItem().toString();
        Category selectedCategory = categoryMap.get(selectedCategoryName);

        Log.d(TAG, "id of selectedCategory " + selectedCategory.getId());

        LiveData<List<Recipe>> recipesWithCategoryLiveData = recipeViewModel.getRecipesByCategoryId(selectedCategory.getId());
        recipesWithCategoryLiveData.observe(this, recipesWithCategory -> {
            Log.d(TAG, "recipesWithCategory size " + recipesWithCategory.size());
            if (recipesWithCategory != null && !recipesWithCategory.isEmpty()) {
                Toast.makeText(this, "Cannot remove the category. It is used in one or more recipes.", Toast.LENGTH_SHORT).show();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Remove Category");
                builder.setMessage("Are you sure you want to remove the selected category?");
                builder.setPositiveButton("Yes", (dialog, which) -> {
                    categoryViewModel.deleteCategoryByName(selectedCategoryName);
                    Toast.makeText(this, "Category removed: " + selectedCategoryName, Toast.LENGTH_SHORT).show();
                    loadCategories(); // Update the spinner with the latest list of categories
                });
                builder.setNegativeButton("No", (dialog, which) -> dialog.cancel());
                builder.show();
            }
        });
    }


    private void loadCategories() {
        categoryViewModel = new ViewModelProvider(this).get(CategoryViewModel.class);
        spinnerCategory = findViewById(R.id.spinnerCategory);

        // Observe the LiveData list of categories from the CategoryViewModel
        categoryViewModel.getAllCategories().observe(this, categories -> {
            if (categories != null && !categories.isEmpty()) {
                List<String> categoryNames = new ArrayList<>();
                for (Category category : categories) {
                    categoryNames.add(category.getName());
                    categoryMap.put(category.getName(), category); // Store the category in the HashMap
                }
                updateSpinner(categoryNames);
            }
        });

        // Set a default value in case the categories haven't loaded yet
        updateSpinner(Collections.singletonList("Loading categories..."));
    }

    private void updateSpinner(List<String> categories) {
        categoryAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categories);
        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCategory.setAdapter(categoryAdapter);
    }


}