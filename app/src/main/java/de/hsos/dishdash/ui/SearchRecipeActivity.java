package de.hsos.dishdash.ui;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import de.hsos.dishdash.R;
import de.hsos.dishdash.adapter.SearchResultsAdapter;
import de.hsos.dishdash.db.entity.Recipe;
import de.hsos.dishdash.viewmodel.RecipeViewModel;

public class SearchRecipeActivity extends AppCompatActivity implements SearchResultsAdapter.RecipeClickListener, SearchResultsAdapter.RecipeBookmarkClickListener {

    private static final String TAG = SearchRecipeActivity.class.getName();

    private RecyclerView rvSearchResults;
    private TextView tvResults;
    private SearchResultsAdapter searchResultsAdapter;
    private RecipeViewModel recipeViewModel;
    private int currentUserId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_search_recipe);

        searchResultsAdapter = new SearchResultsAdapter(this, this); // Pass the click listeners to the adapter
        recipeViewModel = new ViewModelProvider(this).get(RecipeViewModel.class);

        // Get the search query from the MainActivity's intent
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra("SEARCH_QUERY")) {
            String searchQuery = intent.getStringExtra("SEARCH_QUERY");
            // Get the userId from the intent extras
            currentUserId = intent.getIntExtra("USER_ID", -1);

            tvResults = findViewById(R.id.tvResults);
            rvSearchResults = findViewById(R.id.rvSearchResults);

            // Set the "Results for" text
            tvResults.setText(getString(R.string.results_for_format, searchQuery));
            // Load the search results from the database and update the RecyclerView
            loadSearchResults(searchQuery);

            // Back icon click listener
            ImageView ivBack = findViewById(R.id.ivBack);
            ivBack.setOnClickListener(view -> finish());

        } else {
            // If the search query is not provided, finish the activity and return to MainActivity
            finish();
        }
    }

    @Override
    public void onRecipeClick(int position) {
        // Handle click for the whole item (if needed)
        Recipe clickedRecipe = searchResultsAdapter.getRecipeAtPosition(position);
        // Start the ShowRecipeActivity with the recipe data
        Intent intent = new Intent(SearchRecipeActivity.this, ShowRecipeActivity.class);
        // Pass the recipe data to the EditRecipeActivity using intent extras
        // For example:
        intent.putExtra("RECIPE_ID", clickedRecipe.getId());
        intent.putExtra("USER_ID", currentUserId); // Pass the userId as an intent extra
        startActivity(intent);

    }

    @Override
    public void onRecipeBookmarkClick(int position) {
        // Handle click for favorite icon
        Recipe clickedRecipe = searchResultsAdapter.getRecipeAtPosition(position);
        // Toggle the bookmarked status of the recipe in the database and update the icon accordingly
        //Log.d(TAG,"onRecipeBookmarkClick bookmarkLiveData: ");

        recipeViewModel.toggleRecipeBookmark(clickedRecipe.getId());
        clickedRecipe.setBookmarked(!clickedRecipe.isBookmarked());
        searchResultsAdapter.notifyDataSetChanged();
    }

    private void loadSearchResults(String searchQuery) {
        LiveData<List<Recipe>> recipesLiveData = recipeViewModel.getRecipesBySearchQuery(searchQuery);
        recipesLiveData.observe(this, new Observer<List<Recipe>>() {
            @Override
            public void onChanged(List<Recipe> recipes) {
                Log.d(TAG,"updateSearchResults recipes size: " + recipes.size());
                // Update the RecyclerView with the new list of recipes
                searchResultsAdapter.setData(recipes);
                setUpLayOut();
                recipesLiveData.removeObserver(this); // Remove the observer after receiving the data
            }
        });
    }

    private void setUpLayOut() {
        // Set the click listener for the MainCategoryAdapter

        // Initialize the RecyclerView and its adapter
        rvSearchResults.setLayoutManager(new LinearLayoutManager(this));
        rvSearchResults.setAdapter(searchResultsAdapter);
    }

}