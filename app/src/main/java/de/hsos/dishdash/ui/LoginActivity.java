package de.hsos.dishdash.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import de.hsos.dishdash.R;
import de.hsos.dishdash.db.entity.User;
import de.hsos.dishdash.viewmodel.UserViewModel;

public class LoginActivity extends AppCompatActivity {

    public final static String USER_NAME =
            "de.hso.prog3.ss2023.dishdash.LoginActivity.USER_NAME";
    private static final String TAG = LoginActivity.class.getName();
    private Button btnLogin;
    private TextView tvSignUp;
    private EditText inputUsername;
    private EditText inputPassword;

    private UserViewModel userViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("Login");

        this.userViewModel = new ViewModelProvider(this).get(UserViewModel.class);

        // Initialize views
        inputUsername = findViewById(R.id.inputUsername);
        inputPassword = findViewById(R.id.inputPassword);
        btnLogin = findViewById(R.id.btnLogin);
        tvSignUp = findViewById(R.id.tvSignup);

        // Handle click on Login button
        btnLogin.setOnClickListener(v -> loginUser());

        // Handle click on Signup text view
        tvSignUp.setOnClickListener(v -> startActivity(new Intent(LoginActivity.this, RegisterActivity.class)));
    }

    private void loginUser() {
        String username = inputUsername.getText().toString().trim();
        String password = inputPassword.getText().toString().trim();

        if (username.isEmpty() || password.isEmpty()) {
            Toast.makeText(LoginActivity.this, "Please enter username and password", Toast.LENGTH_SHORT).show();
            return;
        }

        LiveData<User> userLiveData = userViewModel.getUserByUserName(username);

        userLiveData.observe(this, new Observer<User>() {
            @Override
            public void onChanged(User user) {
                if (user != null) {
                    Log.d(TAG,"OnClickListener user: " + user.getUsername());
                    if (user.getPassword().equals(password)) {
                        Toast.makeText(LoginActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra(USER_NAME, username);
                        Log.d(TAG,"OnClickListener get UserId: " + user.getId());
                        intent.putExtra("USER_ID", user.getId()); // Pass the userId through the intent
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(LoginActivity.this, "Login Failed! Invalid password", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(LoginActivity.this, "Login Failed! Invalid username", Toast.LENGTH_SHORT).show();
                }
                userLiveData.removeObserver(this); // Remove the observer after receiving the data
            }
        });

    }
}
